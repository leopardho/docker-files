# Check IP
```
docker-machine ip default
```

# Rebuild Container
```
docker-compose stop
docker-compose build
docker-compose up -d
```
If you got this error when running docker-compose commands:
> ERROR: SSL error: hostname 'XXX.XXX.XXX.XXX' doesn't match 'localhost'

Run commands prefixed like this
```
CURL_CA_BUNDLE= docker-compose stop
```

# Remove unused images
```
docker rmi -f $(docker images | grep "<none>" | awk "{print \$3}")
```
